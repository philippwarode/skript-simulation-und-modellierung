% $ biblatex auxiliary file $
% $ biblatex version 2.4 $
% Do not modify the above lines!
%
% This is an auxiliary file used by the 'biblatex' package.
% This file may safely be deleted. It will be recreated as
% required.
%
\begingroup
\makeatletter
\@ifundefined{ver@biblatex.sty}
  {\@latex@error
     {Missing 'biblatex' package}
     {The bibliography requires the 'biblatex' package.}
      \aftergroup\endinput}
  {}
\endgroup

\entry{barroso_web_2003}{article}{}
  \name{author}{3}{}{%
    {{}%
     {Barroso}{B.}%
     {L.A.}{L.}%
     {}{}%
     {}{}}%
    {{}%
     {Dean}{D.}%
     {J.}{J.}%
     {}{}%
     {}{}}%
    {{}%
     {Holzle}{H.}%
     {U.}{U.}%
     {}{}%
     {}{}}%
  }
  \keyw{Websuche}
  \strng{namehash}{BLDJHU1}
  \strng{fullhash}{BLDJHU1}
  \field{sortinit}{B}
  \verb{doi}
  \verb 10.1109/MM.2003.1196112
  \endverb
  \field{issn}{0272-1732}
  \field{number}{2}
  \field{pages}{22\bibrangedash 28}
  \field{shorttitle}{Web search for a planet}
  \field{title}{Web search for a planet: {The} {Google} cluster architecture}
  \field{volume}{23}
  \field{journaltitle}{IEEE Micro}
  \field{month}{03}
  \field{year}{2003}
\endentry

\entry{berninghaus_strategische_2010}{book}{}
  \name{author}{3}{}{%
    {{}%
     {Berninghaus}{B.}%
     {Siegfried}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Ehrhart}{E.}%
     {Karl-Martin}{K.-M.}%
     {}{}%
     {}{}}%
    {{}%
     {Guth}{G.}%
     {Werner}{W.}%
     {}{}%
     {}{}}%
  }
  \list{language}{1}{%
    {Deutsch}%
  }
  \list{publisher}{1}{%
    {Springer}%
  }
  \keyw{Spieltheorie}
  \strng{namehash}{BSEKMGW1}
  \strng{fullhash}{BSEKMGW1}
  \field{sortinit}{B}
  \field{edition}{3}
  \field{isbn}{978-3-642-11650-6}
  \field{shorttitle}{Strategische {Spiele}}
  \field{title}{Strategische {Spiele}: {Eine} {Einführung} in die
  {Spieltheorie}}
  \field{month}{03}
  \field{year}{2010}
\endentry

\entry{brin_anatomy_1998}{inproceedings}{}
  \name{author}{2}{}{%
    {{}%
     {Brin}{B.}%
     {Sergey}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Page}{P.}%
     {Lawrence}{L.}%
     {}{}%
     {}{}}%
  }
  \list{publisher}{1}{%
    {Elsevier Science Publishers B. V.}%
  }
  \keyw{Websuche}
  \strng{namehash}{BSPL1}
  \strng{fullhash}{BSPL1}
  \field{sortinit}{B}
  \field{booktitle}{Proceedings of the {Seventh} {International} {Conference}
  on {World} {Wide} {Web} 7}
  \field{pages}{107\bibrangedash 117}
  \field{series}{{WWW}7}
  \field{title}{The {Anatomy} of a {Large}-scale {Hypertextual} {Web} {Search}
  {Engine}}
  \verb{url}
  \verb http://dl.acm.org/citation.cfm?id=297805.297827
  \endverb
  \list{location}{1}{%
    {Amsterdam, The Netherlands, The Netherlands}%
  }
  \field{year}{1998}
  \field{urlday}{12}
  \field{urlmonth}{02}
  \field{urlyear}{2016}
\endentry

\entry{Griebel_Numeric_Sim}{book}{}
  \name{author}{3}{}{%
    {{}%
     {Griebel}{G.}%
     {Michael}{M.}%
     {}{}%
     {}{}}%
    {{}%
     {Knapek}{K.}%
     {Stephan}{S.}%
     {}{}%
     {}{}}%
    {{}%
     {Zumbusch}{Z.}%
     {Gerhard}{G.}%
     {}{}%
     {}{}}%
  }
  \list{language}{1}{%
    {Englisch}%
  }
  \list{publisher}{1}{%
    {Springer}%
  }
  \keyw{Molekuele}
  \strng{namehash}{GMKSZG1}
  \strng{fullhash}{GMKSZG1}
  \field{sortinit}{G}
  \field{edition}{2007}
  \field{isbn}{978-3-540-68094-9}
  \field{shorttitle}{Numerical {Simulation} in {Molecular} {Dynamics}}
  \field{title}{Numerical {Simulation} in {Molecular} {Dynamics}: {Numerics},
  {Algorithms}, {Parallelization}, {Applications}}
  \list{location}{1}{%
    {Berlin}%
  }
  \field{month}{08}
  \field{year}{2007}
\endentry

\entry{haveliwala_topic-sensitive_2002}{inproceedings}{}
  \name{author}{1}{}{%
    {{}%
     {Haveliwala}{H.}%
     {Taher~H.}{T.~H.}%
     {}{}%
     {}{}}%
  }
  \list{publisher}{1}{%
    {ACM}%
  }
  \keyw{Websuche}
  \strng{namehash}{HTH1}
  \strng{fullhash}{HTH1}
  \field{sortinit}{H}
  \field{booktitle}{Proceedings of the 11th {International} {Conference} on
  {World} {Wide} {Web}}
  \verb{doi}
  \verb 10.1145/511446.511513
  \endverb
  \field{isbn}{978-1-58113-449-0}
  \field{pages}{517\bibrangedash 526}
  \field{series}{{WWW} '02}
  \field{title}{Topic-sensitive {PageRank}}
  \verb{url}
  \verb http://doi.acm.org/10.1145/511446.511513
  \endverb
  \list{location}{1}{%
    {New York, NY, USA}%
  }
  \field{year}{2002}
  \field{urlday}{12}
  \field{urlmonth}{02}
  \field{urlyear}{2016}
\endentry

\entry{langville_googles_2012}{book}{}
  \name{author}{2}{}{%
    {{}%
     {Langville}{L.}%
     {Amy~N.}{A.~N.}%
     {}{}%
     {}{}}%
    {{}%
     {Meyer}{M.}%
     {Carl~D.}{C.~D.}%
     {}{}%
     {}{}}%
  }
  \list{language}{1}{%
    {Englisch}%
  }
  \list{publisher}{1}{%
    {Princeton University Press}%
  }
  \keyw{Websuche}
  \strng{namehash}{LANMCD1}
  \strng{fullhash}{LANMCD1}
  \field{sortinit}{L}
  \field{isbn}{978-0-691-15266-0}
  \field{shorttitle}{Google's {PageRank} and {Beyond}}
  \field{title}{Google's {PageRank} and {Beyond}: {The} {Science} of {Search}
  {Engine} {Rankings}}
  \field{month}{02}
  \field{year}{2012}
\endentry

\entry{leimkuhler_molecular_2015}{book}{}
  \name{author}{2}{}{%
    {{}%
     {Leimkuhler}{L.}%
     {Ben}{B.}%
     {}{}%
     {}{}}%
    {{}%
     {Matthews}{M.}%
     {Charles}{C.}%
     {}{}%
     {}{}}%
  }
  \list{language}{1}{%
    {Englisch}%
  }
  \list{publisher}{1}{%
    {Springer}%
  }
  \keyw{Molekuele}
  \strng{namehash}{LBMC1}
  \strng{fullhash}{LBMC1}
  \field{sortinit}{L}
  \field{edition}{2015}
  \field{isbn}{978-3-319-16374-1}
  \field{shorttitle}{Molecular {Dynamics}}
  \field{title}{Molecular {Dynamics}: {With} {Deterministic} and {Stochastic}
  {Numerical} {Methods}}
  \field{month}{06}
  \field{year}{2015}
\endentry

\entry{mehlmann_strategische_2007}{book}{}
  \name{author}{1}{}{%
    {{}%
     {Mehlmann}{M.}%
     {Alexander}{A.}%
     {}{}%
     {}{}}%
  }
  \list{language}{1}{%
    {Deutsch}%
  }
  \list{publisher}{1}{%
    {Vieweg+Teubner Verlag}%
  }
  \keyw{Spieltheorie}
  \strng{namehash}{MA1}
  \strng{fullhash}{MA1}
  \field{sortinit}{M}
  \field{edition}{2007}
  \field{isbn}{978-3-8348-0174-6}
  \field{shorttitle}{Strategische {Spiele} für {Einsteiger}}
  \field{title}{Strategische {Spiele} für {Einsteiger}: {Eine}
  verspielt-formale {Einführung} in {Methoden}, {Modelle} und {Anwendungen}
  der {Spieltheorie}}
  \field{month}{05}
  \field{year}{2007}
\endentry

\entry{wikPlanckWirk}{misc}{}
  \list{language}{1}{%
    {de}%
  }
  \keyw{Molekuele}
  \field{sortinit}{P}
  \field{note}{Page Version ID: 150810087}
  \field{title}{Plancksches {Wirkungsquantum}}
  \verb{url}
  \verb https://de.wikipedia.org/w/index.php?title=Plancksches_Wirkungsquantum&
  \verb oldid=150810087
  \endverb
  \field{journaltitle}{Wikipedia}
  \field{month}{01}
  \field{year}{2016}
  \field{urlday}{12}
  \field{urlmonth}{02}
  \field{urlyear}{2016}
\endentry

\entry{schlick_molecular_2010}{book}{}
  \name{author}{1}{}{%
    {{}%
     {Schlick}{S.}%
     {Tamar}{T.}%
     {}{}%
     {}{}}%
  }
  \list{language}{1}{%
    {Englisch}%
  }
  \list{publisher}{1}{%
    {Springer}%
  }
  \keyw{Molekuele}
  \strng{namehash}{ST1}
  \strng{fullhash}{ST1}
  \field{sortinit}{S}
  \field{edition}{2nd ed. 2010}
  \field{isbn}{978-1-4419-6350-5}
  \field{shorttitle}{Molecular {Modeling} and {Simulation}}
  \field{title}{Molecular {Modeling} and {Simulation}: {An} {Interdisciplinary}
  {Guide}}
  \list{location}{1}{%
    {New York}%
  }
  \field{month}{10}
  \field{year}{2010}
\endentry

\entry{wikSchroedGl}{misc}{}
  \list{language}{1}{%
    {de}%
  }
  \keyw{Molekuele}
  \field{sortinit}{S}
  \field{note}{Page Version ID: 150219456}
  \field{title}{Schrödingergleichung}
  \verb{url}
  \verb https://de.wikipedia.org/w/index.php?title=Schr%C3%B6dingergleichung&ol
  \verb did=150219456
  \endverb
  \field{journaltitle}{Wikipedia}
  \field{month}{01}
  \field{year}{2016}
  \field{urlday}{18}
  \field{urlmonth}{01}
  \field{urlyear}{2016}
\endentry

\lossort
\endlossort

\endinput
