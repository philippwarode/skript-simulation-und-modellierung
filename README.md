# Skript Simulation und Modellierung #
## TU Berlin - 2015 ##

Dieses Repository enthält das LaTeX Skript zur Vorlesung Simulation und Modellierung aus dem Wintersemester 2015/16 an der TU Berlin, gehalten von Konstantin Fackeldey.

Der aktuelle Stand (wöchentlich aktualisiert) als PDF befindet sich im Ordner /pdf.

Die Master-LaTeX-Datei ist Skript.tex, der eigentliche Inhalt liegt in den xx_lecture.tex Dateien im Unterordner /lectures.
Aufgaben sind extra im Ordner exercises, werden aber im Skript eingebunden.

## Der aktuelle Stand als PDF ist unter Downloads zu finden ##
