
\marginpar{18.01.2016}
Bevor wir Moleküle genauer modellieren können, benötigen wir noch einige chemische/physikalische Grundbegriffe und ein grundlegendes Verständnis von chemischen Bindungen.

Gehen wir davon aus, dass ein System aus $N$ Atomkernen und $M$ Elektronen besteht. Betrachten wir das Modell des Atoms $j \in \{1, \dotsc, N\}$, hier das eines Natrium Atoms:
\begin{center}
\begin{tikzpicture}
\draw[black, fill=DarkOrchid!20] (0,0) circle (0.5cm);
\draw[dashed, gray] (0,0) circle (1cm)
					(0,0) circle (1.5cm)
					(0,0) circle (2cm);
\node at (0,0) {$Na$};
%Innere Hülle
\fill[Blue] (0,1) circle (0.1)
			(0,-1) circle (0.1);
%Mittlere Hülle			
\fill[Blue] (0,1.5) circle (0.1)
			(1.05,1.05) circle (0.1)
			(1.5,0) circle (0.1)
			(1.05,-1.05) circle (0.1)
			(0,-1.5) circle (0.1)
			(-1.05,-1.05) circle (0.1)
			(-1.5,0) circle (0.1)
			(-1.05,1.05) circle (0.1);
%Valenzelektron
\fill[Blue!75] (0,2) circle (0.1);
%Legende
\draw[DarkOrchid] (2.5,1) edge[->] (0.4,0.4) node[anchor=west] {Atomkern $j$ mit Koordinaten $R_j$};
\draw[Blue] (2.5,-1) edge[->] (1.2,-1.05) node[anchor=west] {Elektron $i$ mit Koordinaten $r_i$};
\draw[Blue!75] (2.5,2) edge[->] (0.2,2) node[anchor=west] {Valenzelektron};
\draw[black!80] (2.5,0) edge [->] (0.75,-0.7) node[anchor=west] {Schalen/Orbitale};
\draw[black!80] (2.5,0) edge [->] (1.5,0.45);
\draw[black!80] (2.5,0) edge [->] (1.95,0.5);
\end{tikzpicture}
\end{center}
Wir haben also die Kerne mit den Koordinaten $R_j$ und die Elektronen mit den Koordinaten $r_i$. Die Elektronen sind dabei \glqq schalenförmig\grqq{} um den entsprechenden Atomkern angeordnet\footnote{Die Schalen oder Orbitale sind dabei gerade die stationären Zustände aus der zeitunabhängigen Schrödingergleichung \eqref{Schroedinger_Gleichung_zeitunab} mit zugehörigen Energieleveln.}. Die äußerste Schale/das äußerste Orbital nennt man Valenzschale. Die darauf befindlichen Elektronen nennt man dementsprechend Valenzelektronen. Diese sind entscheidend für die Bindungsfähigkeit von Atomen, denn jedes Atom strebt die sogenannte Edelgaskonfiguration an. Dies ist der sehr stabile Zustand, in dem die äußerste Schale des Atoms mit genau acht Valenzelektronen besetzt ist (Oktettregel).
\subsubsection*{Chemische Bindungen}
Durch Übertragung von Außenelektronen auf den Partner erlangen die Atome die stabile Edelgaskonfiguration. Abhängig von Partner, Elektronenübertragung und Elektronegativität $\Delta E$ betrachten wir folgende Bindungstypen:

\textbf{Atombindung (kovalente oder Elektronenpaarbindung)}\\
Zwei Nichtmetalle teilen sich mindestens ein bindendes  Elektron falls ${\Delta E \leq 1{,}7}$, zum Beispiel bei $H_2 O$. 

\textbf{Ionenverbindung:} \\
Vollständige Übertragung der Außenelektronen an den Bindungspartner falls ${\Delta E>1{,}7}$, zum Beispiel:
\begin{center}
\begin{tikzpicture}
\node (1) at (0,0) {$Na$};
\node (2) at (0.75,0) {$+$};
\node[outer sep=0.2cm] (3) at (1.5,0) {$Cl$};
\node[outer sep=0.2cm] (4) at (4,0) {$Na^+$};
\node (5) at (5.52,0) {$Cl^-$};
%Valenzelektronen linke seite
\fill[Blue] (0.4,0) circle (0.075)
			(1.1,0.1) circle (0.075)
			(1.1,-0.1) circle (0.075)
			(1.4,-0.4) circle (0.075)
			(1.6,-0.4) circle (0.075)
			(1.4,0.4) circle (0.075)
			(1.6,0.4) circle (0.075)
			(1.9,0) circle (0.075);
%Valenzelektronen rechte seite
\fill[Blue] (5.05,0.1) circle (0.075)
			(5.05,-0.1) circle (0.075)
			(5.95,0.1) circle (0.075)
			(5.95,-0.1) circle (0.075)
			(5.4,-0.45) circle (0.075)
			(5.6,-0.45) circle (0.075)
			(5.4,0.45) circle (0.075)
			(5.6,0.45) circle (0.075);			
\draw (3) edge[->] (4);
\end{tikzpicture}
\end{center}

\subsubsection*{Born-Oppenheimer Approximation}
Um die Schrödingergleichung leichter lösen zu können, wollen wir einige Annahmen treffen. Das unterschiedliche Größe/Masseverhältnis zwischen den Elektronen und den Kernen wird dabei verwendet, um die Schrödingergleichung in einen Teil für die Kerne und einen Teil für die Elektronen aufzuteilen. 

Für die Modellierung von Molekülen verwenden wir für $H$ den sogenannten molekularen Hamilton:
\begin{equation}
H = T_N(R) + T_e(r) + V_{eN} (r,R) + V_{NN}(R) + V_{ee} (r) \label{Hmol}
\end{equation}
wobei $T$ die kinetischen und $V$ die potentiellen Energie zwischen den Partikeln bezeichnet. Dabei ist
\begin{itemize}
\item[$T_N$] die kinetische Energie der Kerne
\item[$T_e$] die kinetische Energie der Elektronen
\item[$V_{eN}$] die potentielle Energie, die durch die Anziehung von Kernen und Elektronen entsteht
\item[$V_{NN}$] die potentielle Energie, die durch die Abstoßung der Kerne entsteht
\item[$V_{ee}$] die potentielle Energie, die durch die Abstoßung der Elektronen entsteht
\end{itemize}

Wir nehmen nun an, dass der Zustand der Kerne aufgrund der großen Trägheit relativ zu den schnellen Bewegungen der Elektronen nur von den Koordinaten der Kerne selbst abhängt. Den Zustand der Kerne beschreiben wir mit $\chi (R)$. Der Zustand der Elektronen ist weiterhin von den Koordinaten der Kerne wie auch der Elektronen abhängig und wird mit $\phi(r, R)$ bezeichnet. \\
Grundlage der Born-Oppenheimer Approximation ist nun, dass wir annehmen, dass unser Gesamtzustand sich als Produkt der beiden Einzelzustände schreiben lässt. Wir nehmen also an, dass
\begin{equation}
\psi (r,R) \simeq \chi(R) \cdot \phi(r, R) \label{Born_Oppenheimer_Approx}
\end{equation}
Die Bewegungen der Elektronen um die Kerne ist vergleichsweise sehr schnell. Deshalb nehmen wir zur Vereinfachung an, dass für den Zustand der Elektronen die Kernpositionen als konstant angesehen werden können. \\
Mit \eqref{Born_Oppenheimer_Approx} und H wie in \eqref{Hmol} erhalten wir aus der zeitunabhängigen Schrödingergleichung \eqref{Schroedinger_Gleichung_zeitunab} für festes $R$\footnote{Der Term $\chi(R)$ ist für festes $R$ konstant und kann deshalb vernachlässigt werden.}
\begin{equation*}
[ \underbrace{T_N(R) + V_{NN}(R)}_{=:E_N(R)} + T_e(r) + V_{eN} (r,R) +  V_{ee} (r) ] \phi(r, R) = E \phi(r, R)
\end{equation*}
und schließlich mit $E_{\text{el}}(R) := E - E_N(R)$ eine \textit{Schrödingergleichung für die Elektronen} bei gegebener Position $R$ der Kerne
\begin{equation}
[ T_e(r) + \underbrace{V_{eN} (r, R)}_{\text{\glqq Pseudopotential\grqq}} + V_{ee} (r) ] \phi(r, R) =
 E_{\text{el}} (R) \phi( r, R ) \label{VL25_1}
\end{equation}
In \eqref{VL25_1} betrachtet man üblicherweise nur die Valenzelektronen, also die Elektronen auf der äußersten Schale. \\

Andereseits erhalten wir aus \eqref{Schroedinger_Gleichung_zeitunab} mit $H$ wie in \eqref{Hmol} und der Born-Oppenheimer Approximation \eqref{Born_Oppenheimer_Approx} und der Gleichung \eqref{VL25_1} die \textit{Schrödingergleichung für die Kerne}:
\begin{equation}
[ T_N (R) + \underbrace{V_{NN} (R) + E_{\text{el}} (R)}_{=:U^{\text{BO}}} ] \chi(R) = E_{\chi} \chi(R) \label{VL25_2}
\end{equation}
Dabei ist die Variable $r$ nur noch implizit im Term $E_{\text{el}}$ enthalten. Dieser ist selbst Teil der potentiellen Energie $U^{\text{BO}}$ dieser Gleichung. Wir nennen 
\begin{equation}
U^{\text{BO}} := V_{NN} (R) + E_{\text{el}} (R)
\end{equation}
das \textbf{Born-Oppenheimer Potential}.

Die Dynamik der Kerne können wir also schließlich mit demselben Hamilton-Operator wie in \eqref{VL25_2} mit der zeitabhängigen Schrödingergleichung \eqref{Schroedinger_Gleichung} beschreiben.
\begin{equation*}
[ T_N (R) + V_{NN} (R) + E_{el} (R) ] \chi (R,t) = i \hbar \frac{\partial \chi}{\partial t}
\end{equation*}
Würde man nun $U^{BO}$ in einer Simulation berechnen, müsste man für jede Bewegung der Kerne die elektronische Schrödingergleichung \eqref{VL25_1} neu berechnen, was zu aufwändig wäre. Man nimmt daher an, dass die Elektronen (aufgrund des Größenunterschiedes) in der Lage sind, den \glqq trägen\grqq{} Kernen zu folgen und somit immer auf dem gleichen Energielevel bleiben. \\
Dann benutzt man eine Taylorentwicklung für $U^{BO}$ und beschreibt die Dynamik mit der Newtonschen Bewegungsgleichung.

\subsubsection{Wechselwirkungspotentiale}
\textit{Ziel:} Beschreibe die Energie des Systems als Funktion der Kernkoordinaten, also wollen wir
\begin{equation*}
U^{BO} (q) \approx V(q) = \sum_i v_1 (q_i) + \sum_{i,j} v_2 (q_i, q_j) + \sum_{i,j,k} v_3(q_i, q_j, q_k) + \dots
\end{equation*}
wobei ab jetzt $q_i$ die Koordinaten des $i$-ten Atoms beschreibt. 