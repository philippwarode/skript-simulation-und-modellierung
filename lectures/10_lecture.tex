\marginpar{10.11.2015}
Ein Markovprozess ist homogen, falls die Übergangswahrscheinlichkeiten
\[ \Prob [X_{t_{k+1}} = x_{k+1} | X_{t_k} = x_k ] \] sind bezüglich der Zeit translationsinvariant. Das heißt die Wahrscheinlichkeiten hängen nur von der Differenz $t_{k+1} = t_k$ ab. \\
\textit{Zustandswahrscheinlichkeiten:} $\pi^{(k)} = (\pi_1^{(k)}, \dots , \pi^{(k)}_n)$ ist der Vektor der Zustandswahrscheinlichkeiten, wobei $\pi^{(k)}_i := \Prob [X_k = i]$.

\textbf{Anwendung auf den Zufallssurfer:}\\
Dabei ist $\pi_i$ der Page-Rank der Seite $S_i$ und gibt die Wahrscheinlichkeit an, mit der sich ein Benutzer auf Seite $S_i$ befindet. \\
$\Prob [X_{k+1} = S_j | X_k = S_i ]$ ist die Wahrscheinlichkeit im $k$-ten Zeitschritt von einer Seite $S_i \in E$ auf $S_j \in E$ zu wechseln (Unabhängig vom Zeitpunkt $k$).
\begin{align*}
\Prob [X_1 = S_j | X_0 = S_i ] =
\begin{cases}
\frac{1}{|S_i|}		& S_i \text{ besitzt einen Link zu } S_j \\
\frac{1}{n}			& S_i \text{ besitzt überhaupt keinen Link} \\
0					& \text{sonst}
\end{cases}
\end{align*}
Dann ist \[\pi_j^{(k)} = \Prob [X_k = S_j] \]
die Wahrscheinlichkeit, dass der Zufallssurfer im $k$-ten Schritt auf $S_j$ ist. \\
Damit können wir die \textbf{Übergangsmatrix der Markov-Kette} aufstellen:
\begin{equation*}
U \in \R^{n,m} \qquad u_{ij} := \Prob [X_1 = S_j | X_0 = S_i ]
\end{equation*}
Die Matrix $U$ ist eine \textbf{stochastische Matrix}, das heißt
\[ u_{ij} \geq 0 \text{ und } \sum_{j=1}^n u_{ij} =1 \]
Somit folgt aus der Markov-Eigenschaft, dass durch die Angabe einer Anfangsverteilung $\pi^{(0)}$ alle Verteilungen $\pi^{(k)}$ zu einem \glqq späteren\grqq{} Zeitpunkt festgelegt sind.
\begin{equation*}
\left( \pi^{(k)} \right)^T
 = \left( \pi^{(k-1)} \right)^T U
 = \dotsc 
 = \left( \pi^{(0)} \right)^T U^k
\end{equation*}

\begin{minipage}[t]{7.5cm}
\textbf{Vorherige Formulierung:}\\
Wir hatten die Matrix $H$ betrachtet mit
\begin{equation*}
h_{ij} = 
\begin{cases}
\frac{1}{|S_i|} & S_i \text{ mit Link auf }S_j \\
0 & \text{sonst}
\end{cases}
\end{equation*}
und das Gleichungssystem
\[ H^T r = r \]
\end{minipage}
\hspace{0.5cm}
\begin{minipage}[t]{7cm}
\textbf{Markov Formulierung:}\\
Nun haben wir die Matrix $U$ mit
\begin{equation*}
u_{ij} =
\begin{cases}
\frac{1}{|S_i|}		& S_i \text{ mit Link zu } S_j \\
\frac{1}{n}			& S_i \text{ hat keinen Link} \\
0					& \text{sonst}
\end{cases}
\end{equation*}
und das Gleichungssystem
\[ \left( \pi^{(k)} \right)^T = \left( \pi^{(0)} \right)^T U^k \]
\end{minipage}

Als Zusammenhang erkennen wir
\begin{equation*}
U = H + \frac{1}{n} + a e^T
\end{equation*}
wobei
\begin{align*}
e &= (1, \dotsc, 1)^T \\
a \in \R^n \text{ mit } a_i &=
\begin{cases}
1 & S_i \text{ besitzt keinen Link} \\
0 & S_i \text{ besitzt mindestens einen Link}
\end{cases}
\end{align*}
\textbf{Frage:}\\
Wie erhalten wir aus dieser Markovkette nun unseren Page-Rank-Vektor $\pi \in \R^n$, das heißt die Wahrscheinlichkeitsverteilung, die angibt mit welcher Wahrscheinlichkeit sich der Websurfer auf welcher Website befindet. \vspace{0.2cm} \\
\textit{Wir haben:} $\left( \pi^{(k)} \right)^T = \left( \pi^{(0)} \right)^T U^k $ \\
\textit{Wir wollen:} $\pi^T = \pi^T U$ 

Dieses $\pi$ sollte die folgende Eigenschaft haben:
\begin{enumerate}[(i)]
\item Die Zahl $\pi_i$ soll die mittlere Aufenthaltszeit eines Surfers auf Webseite $S_i$ beschreiben
\begin{equation*}
\pi_i = \lim_{l \to \infty} \left( \frac{1}{l} \sum_{k=0}^{l-1} \pi_i^{(k)} \right)
\end{equation*}
\item Starten wir unseren Prozess mit $\pi_0 = \pi$, dann sollte 
\begin{equation*}
\pi = \pi^{(0)} = \dotsc = \pi^{(k)} \text{ also } U^T \pi = \pi
\end{equation*}
gelten.
\item Starten wir mit einer beliebigen Anfangsverteilung $\pi^{(0)}$, so sollte $\pi^{(k)}$ für größer werdendes $k$ gegen $\pi$ streben:
\begin{equation*}
\pi^T = \lim_{k \to \infty} \left( \pi^{(k)} \right)^T = \left( \pi^{(0)} \right)^T \lim_{k \to \infty} U^k
\end{equation*}
\end{enumerate}
Also ist der Page-Rank-Vektor $\pi$ der Eigenvektor von $U^T$ zum Eigenwert $\lambda = 1$. Wir suchen also jetzt nach der stationären Verteilung der Markov-Kette. Wir erkennen das wir ähnliche Eigenschaften (sogar mehr) haben, als im ersten Ansatz. Insbesondere haben wir hier das Problem mit \glqq Senken\grqq{} im Graphen behoben.

\textbf{Frage:} Was ist mit der Existenz und Eindeutigkeit der Lösung von \[ U^T \pi = \pi \]
das heißt: \glqq Macht $\pi^{(k+1)} = U^T \pi^{(k)}$ mit $\pi^{(k+1)} = \left( U^{(k)} \right)^T \pi$ Sinn?\grqq
%
\subsection{Perron-Frobenius-Theorie}
Betrachte zunächst allgemein asymptotisches Verhalten:
\begin{equation*}
x^{k+1} = A x^k \qquad k = 0,1,2, \dotsc
\end{equation*}
Falls $v$ Eigenvektor von $A$ ist (zum Eigenwert $\lambda \in \C$) so erhalten wir für den Anfangszustand $x^0 = v$ die Lösung
\begin{equation*}
x^{k+1} = \lambda^k v \qquad k = 0,1,2, \dotsc
\end{equation*}
Ist $A$ diagonalisierbar, existiert also eine Basis aus Eigenvektoren $v_1, \dotsc, v_n$ mit zugehörigen Eigenwerten $\lambda_1, \dotsc, \lambda_n$ besitzt, dann lässt sich jeder beliebige Anfangszustand $x^0$ als Linearkombination der $v_i$ darstellen
\begin{equation*}
x^0 = \beta_1 v_1 + \dots + \beta_n v_n \qquad \beta_i \in \C
\end{equation*}
Die Lösung ist dann
\begin{equation} \label{loesung_x_k_eigenbasis}
x^k = \lambda_1^k \beta_1 v_1 + \dotsc + \lambda_n^k \beta_n v
\end{equation}
Daraus erkenne wir: Für große $k$ (also lange Zeiten) setzen sich Eigenvektoren mit betragsmäßig großen Eigenwerten durch.
\begin{thm} \label{Satz_4_1}
Sei $\lambda_1$ ein einfacher und strikt dominanter Eigenwert (d.h. $|\lambda_1| = 1$ eindeutig) der Matrix $\C^{n \times n}$ mit dem zugehörigen (Rechts-)Eigenvektor $v_1$ und Linkeigenvektor $w_1$ mit $w_1^T v_1 = 1$. Dann gilt für jede Lösung von \[ x^{k+1} = A x^k \qquad k=0,1,2,\dotsc \]
dass
\begin{equation*}
\lim_{k \to \infty} \frac{x^k}{\lambda_1^k} = (w_1^T x^0 ) v_1
\end{equation*}
\end{thm}
\begin{proof}
Wir führen den Beweis für diagonalisierbare Matrizen $A$ durch (der allgemeine Beweis ist möglich mit Hilfe der Jordan-Normalform). \\
Sei also $A$ diagonalisierbar mit Eigenwerten $\lambda_1, \dotsc, \lambda_n$ und $|\lambda_1| > |\lambda_i|$, $i=2, \dots, n$, dann folgt
\begin{equation*}
\lim_{k \to \infty} \frac{\lambda_i^k}{\lambda_1^k} = 0 \qquad i=2,3,4, \dotsc
\end{equation*}
Zusammen mit \eqref{loesung_x_k_eigenbasis} folgt also
\begin{equation*}
\lim_{k \to \infty} \frac{x^k}{\lambda_1^k} = \beta_1 v_1
\end{equation*}
Für die (Rechts-)Eigenvektoren $v_1, \dotsc, v_n$ von A gilt
\begin{equation*}
w_1^T A v_i = w_1^T \lambda_i v_i = \lambda_1 w_1^T v_i
 \Rightarrow
(\lambda_i - \lambda_1) w_1^T v_i = 0
\end{equation*}
Wegen $\lambda_i - \lambda_1 \neq 0$ folgt $w_1^T v_2 = \dots = w_1^T v_n = 0$ und damit
\[ w_1^T x^0 = \beta_1 w_1^T v_1 = \beta_1 \]
\end{proof}