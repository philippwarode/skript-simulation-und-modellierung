\marginpar{16.11.2015}
\begin{defn}
Eine Matrix $A$ heißt \textbf{positiv}, falls alle Einträge in $A$ positiv sind. \\
$\sigma(A) = \{ \lambda \C | Ker(A - \lambda I) \neq 0 \}$ heißt \textbf{Spektrum} der Matrix $A$. \\
$\rho(A) = \max \{|\lambda| | \lambda \in \sigma(A) \}$ heißt \textbf{Spektralradius} von $A$.
\end{defn}
\begin{itemize}
\item 
Es gilt
\[ \lim_{k \to \infty} A^k = 0 \Leftrightarrow \rho(A) < 1 \]
\item
Für $A \in \R^{n \times m}, A>0$ gilt $\rho(A)>0$ \\
Wir nehmen deshalb o.B.d.A. $\rho(A) =1$.
\end{itemize}
\begin{thm}[Satz von Perron] \label{Perron}
Sei $A>0$ mit Spektralradius $\rho(A)=1$, dann gilt
\begin{enumerate}[(a)]
\item Der Spektralradius $\rho(A)=1$ ist Eigenwert
\item Der Eigenwert $\lambda = 1$ ist der einzige auf dem Einheitskreis
\item Zu $\lambda = 1$ exisiteren positive Rechts- und Linkseigenvektoren
\item Der Eigenwert $\lambda=1$ hat algebraische Vielfachheit 1
\item Außer dem Rechtseigenvektor $v_1$ gibt es keine weiteren nicht negativen Eigenvektoren von $A$ (bis auf skalare Vielfache)
\end{enumerate}
\end{thm}
\begin{proof}~
\begin{enumerate}[(a)]
\item Betrachte zunächst mögliche Eigenvektoren $x$ zum Eigenwert $\lambda$ auf dem Einheitskreis $|\lambda| = 1$. Für diese gilt
\begin{equation} \label{proof_perron1}
|x| = |\lambda| |x| = |\lambda x | = |Ax| \leq |A| |x| = A |x|
\end{equation} 
Falls solche Eigenvektoren $x \neq 0$ existieren, so gilt
\[ z := A |x| > 0 \]
Definiere $y:=z - |x|$, dann kann man \eqref{proof_perron1} zu
$y \geq 0$ umformulieren. \\
Nehmen wir nun an, dass $y \neq 0$ gelten würde, so folgt
\[ Ay > 0 \]
Dann existiert auch $\tau>0$, sodass $Ay > \tau z$ womit folgt
\begin{equation*}
 Ay = Az - A|x| = Az - z > \tau z  \Leftrightarrow Az > (1+\tau) z
\end{equation*}
Setze $B := \frac{1}{1+\tau} A$, dann gilt
\[ Bz > z \]
Wiederholtes Anwenden ergibt schließlich
$B^k z > z$ und mit $\rho(B) = \frac{1}{1+\tau} < 1$ folgt
\[ \lim_{k \to \infty} B^k z = 0 > z \]
Dies steht im Widerspruch zu $z>0$ bzw. $y \neq 0$ \hfill $\lightning$ \\
Also ist 
\[y = A|x| - |x| = 0 \]
und damit existiert ein Eigenwert $\lambda = 1$
%
%
\item Die Widerspruchsannahme $y \neq 0$ ($A|x| - |x| \neq 0$) schloss $\lambda = 1$ mit ein. Somit ist $\lambda = 1 \in \R$  einziger Eigenwert auf dem Einheitskreis.
%
%
\item Wegen $|x| = A|x| > 0$ sind alle Komponenten positiv. Dies gilt auch für Links- Rechtseigenvektoren gleichermaßen, da auch $A^T$ positiv ist
%
%
\item \textit{Übung}
\item \textit{Übung}
\end{enumerate}
\end{proof}
Der Wert $\lambda = \rho(A)$ wird auch \textbf{Perron-Eigenwert} genannt.
\begin{cor}
Aus Satz \ref{Perron} folgt für eine Markovkette mit positiver Übergangsmatrix {$U>0$} die Existenz einer eindeutigen stationären Verteilung. \\
Mit Satz \ref{Satz_4_1} zusammen können wir folgende Aussage über das asymptotische Verhaten der positiven Übergangsmatrix $U>0$ treffen: \\
Wir können zu $\lambda = 1$ den  Vektor $e = (1, \dotsc, 1)^T$ wählen. Dann gilt $e^T \pi = 1$. Somit $\lim_{k \to \infty} \pi^k = (e^T \pi^0) \pi = \pi$
\end{cor}
Leider lässt sich der Satz von Perron nicht auf die Übergangsmatrix $U$ des Zufallssurfers angewendenden, da $U$ nur nicht-negativ ist. \\
Wie kann den Satz also auf nicht-negative Matrizen erweitern? Die Antwort darauf liefert der Satz von Frobenius. Dafür müssen wir zunächst Irreduzibilität definieren.
\begin{defn}[Irreduzibel]
Sei $A \in \R^{n \times n}$. \\
Eine Matrix $P \in \R^{n \times n}$ heißt \textbf{Permutationsmatrix}, falls $p_{ij} \in \{0,1\}$ und jede Spalte und Zeile genau eine Eins enthält. \\
$A$ heißt \textbf{reduzibel}, falls es eine Permutationsmatrix $P$ gibt, sodass
\[ P^T A P = 
\begin{pmatrix}
C & D \\
0 & F 
\end{pmatrix}  \]
wobei die Blockmatrizen $C$ und $F$ quadratisch sein müssen. \\
Falls kein solcher \textit{Nullblock} existiert, so ist die Matrix \textbf{irreduzibel}.
\end{defn}
\begin{rem}
Im gerichteten Graphen ist eine Permutation nur eine Umnummerierung der Kanten. \\
Ein Graph ist irreduzibel (oder auch stark zusammenhängend), falls von jedem Knoten zu jedem anderen Knoten ein zusammenhängender Pfad existiert.
\end{rem}
\begin{lem}[Charakterisierung von Irreduzibilität]
Falls $A \in \R^{n \times n}, A \geq 0$ irreduzibel ist, dann gilt
\[ (I + A)^{n-1} > 0 \]
\end{lem}
\begin{proof}
Seien $A^k = (a_{ij}^{(k)} )_{i,j = 1, \dotsc, k}$ die Potenzen der nicht-negativen Matrix $A$. Elementweise gilt dann
\begin{equation*}
a_{ij}^{(k)} = \sum_{l_1, \dotsc, l_k} a_{i l_1} \cdot, a_{l_1 l_2} \dots a_{l_{k-1} j}
\end{equation*}
Diese Elemente verschwinden, falls mindestens einer der Faktoren verschwindet, also falls beim zugehörigen Graphen kein Pfad vom Knoten $i$ zu $j$ existiert. \\
Falls jedoch dieser Graph existiert, so ergibt es mindestens eine Indexfolge $a_{i l^*_1} >0, \dotsc, a_{l^*_{k-1} j} >0$. Bei einer irreduziblen Matrix tritt dieser Fall spätestens nach durchlaufen aller Knoten auf, also $n-1$:
\begin{equation*}
\left[ (I + A)^{n-1} \right]_{ij} = \left[ \sum_{k=0}^{n-1} \begin{pmatrix} n-1 \\ k \end{pmatrix} A^k \right]_{ij} = \sum_{k=0}^{n-1} \begin{pmatrix} n-1 \\ k \end{pmatrix} a_{ij}^{(k)} > 0
\end{equation*}
\end{proof}
\begin{thm}[Perron-Frobenius 1915]
Es sei $A \geq 0$ eine irreduzible stochastische Matrix. Dann gilt:
\begin{enumerate}[(a)]
\item Der Perron-Eigenwert $\lambda = 1$ ist einfach
\item Zu $\lambda=1$ existiert ein Linkseigenvektor $\pi^T > 0$
\end{enumerate}
\end{thm}
\begin{proof}
\begin{enumerate}[(a)]
\item
Sei $A$ stochastisch, also $Ae = e$ mit $e^T = (1, \dotsc, 1)^T$ Rechtseigenvektor zum Eigenwert $\lambda = 1$ ist. Es gilt
\begin{equation*}
|\lambda(A)| \leq \rho(A) \leq \Vert A \Vert_\infty \leq 1
\end{equation*}
wobei $\Vert \cdot \Vert_\infty$ die Maximumsnorm ist. \\
Zeige nun: Der Eigenwert ist einfach. Betrachte dazu
\[ B := (I + A)^{n-1} > 0\]
Seien nun $\lambda$ die Eigenwerte von $A$, dann sind die Eigenwerte von $B$ gerade {\nolinebreak $(1+\lambda)^{n-1}$}. \\
\begin{equation} \label{proof_frobenius1}
\mu = \rho(B) = \max_{|\lambda| < 1} |1+ \lambda|^{n-1} = 2^{n-1}
\end{equation}
Dieser Eigenwert ist einfach und liegt auf dem Kreis mit Radius $\mu$. Das Maximum in \eqref{proof_frobenius1} wird für $\lambda = 1$ angenommen. Also sind Multiziplität des Eigenwertes $\mu$ von $B$ und des Eigenwertes $\lambda$ von $A$ gleich.
