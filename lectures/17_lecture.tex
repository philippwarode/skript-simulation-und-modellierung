\marginpar{07.12.15} Die Annahme, dass $\lambda$ konstant ist, ist nicht realistisch.
Wir wählen deshalb $\lambda$ deshalb abhängig von der Relativ-Geschwindigkeit.
 \[ \lambda = \frac{c}{x_n (t) - x_{n-1} (t)} \]
Somit erhalten wir insgesamt
\begin{align*}
\frac{d x_n^2}{dt^2} = c \frac{\frac{d x_n(t)}{d_t} - \frac{d x_{n-1}(t)}{d_t}}{x_n(t) - x_(n-1)(t)}
\end{align*}
Integration bezüglich $t$ liefert nun
\begin{align*}
\frac{d x_n(t)}{dt} = c \ln \left\vert x_n(t) - x_{n-1}(t) \right\vert + d_n
\end{align*}
da nun  $x_n(t) - x_{n-1}(t)$ ein Maß für die Verkehrsdichte $\rho$ bzw $\frac{1}{\rho}$ ist,
\[ v(x,t) = - c \ln \rho + d_n \]
Wir wählen $d_n$ wieder so, dass wir bei $\rho = \rho_{\max}$ $v = 0$ erhalten, also $d_n = c \ln \rho_{\max}$. Zusammengefasst erhalten wir:
\begin{equation*}
v(x,t) = 
\begin{cases}
v_{\max} & \rho \leq \rho_{crit} \\
- c \cdot \frac{\ln \rho(x,t)}{\rho_{\max}} & \rho > \rho_{crit}
\end{cases}
\end{equation*}

\subsubsection*{Lineare Differentialgleichungen - gleichförmiger Verkehr}
Durch unsere Modellierungsannahme $v = v(\rho)$ haben wir nun aus dem Erhaltungssatz \ref{Thm_Differenzieller_Erhaltungssatz}, dass
\[ \frac{\partial \rho}{\partial t} + \frac{\partial}{\partial x} q( \rho ) = 0 \]
mit der Kettenregel wird dies zu
\begin{equation}
\frac{\partial \rho}{\partial t} + q'(\rho) \cdot \frac{\partial \rho}{\partial x} = 0 \label{Erhaltungssatz_nicht_konstant}
\end{equation}
Um die Fortentwicklung des Verkehrs zu berechnen, müssen wir also \eqref{Erhaltungssatz_nicht_konstant} lösen. Dabei kommt es auf die Gestalt von $q'(\rho)$ an. Wir werden zunächst den einfacheren Fall betrachten, dass $q'(\rho)$ konstant ist. Im darauffolgenden Abschnitt werden wir die selbe Untersuchung auch im allgemeinen Fall untersuchen.

Betrachten wir nun also zunächst den \textit{linearen Fall}.
\begin{modeling}
Wir nehmen an, dass wir Verkehr mit  konstanter Dichte $\rho_0$ haben, die nur durch kleine Störungen überlagert wird. Wir haben also
\begin{equation*}
\rho(x,t) = \rho_0 + \epsilon \rho_1(x,t) \qquad \epsilon > 0
\end{equation*}
\end{modeling}
Einsetzen in \eqref{Erhaltungssatz_nicht_konstant} bringt
\[
\frac{\partial}{\partial t} \left( \rho_0 + \epsilon \rho_1(x,t) \right) + q'\left(\rho_0 + \epsilon \rho_1(x,t) \right) \cdot \frac{\partial}{\partial x} \left( \rho_0 + \epsilon \rho_1(x,t) \right) = 0
\]
Division durch $\epsilon$ ergibt:
\[
\frac{\partial \rho_1}{\partial t} + q' \left( \rho_0 + \epsilon \rho_1(x,t) \right) \cdot \frac{\partial \rho_1}{\partial x} = 0
\] 
Verwende für $q'$ die Taylorreihe
\[ q'\left( \rho_0 + \epsilon \rho_1 (x,t) \right) 
= q'(\rho_0) + \epsilon \rho_1  q'' (\rho_0) + \mathcal{O} (\epsilon^2) \]
Vernachlässigung aller Potenzen von $\epsilon$ bringt uns folgende Gleichung für die Störungen:
\[
\frac{\partial \rho_1}{\partial t} + q'(\rho_0) \cdot \frac{\partial \rho_1}{\partial x} = 0
\]
welches eine lineare Differentialgleichung ist, da $q'(\rho_0)$ konstant ist.

Um gleichförmigen Verkehr, dessen konstante Dichte $\rho_0$ nur durch Schwankungen $\rho_1(x,t)$ gestört wird, zu simulieren, müssen wir nun
\begin{equation}
\frac{\partial \rho_1}{\partial t} + c \cdot \frac{\partial \rho_1}{\partial x} = 0 \quad \text{mit} \quad c = q'(\rho_0) \label{Verkehr_Schwankung}
\end{equation}
lösen, um die zeitliche Entwicklung der Störungen von $\rho_0$ zu verfolgen. \\
Nehmen wir also an, wir kennen die Störung zum Zeitpunkt $t=0$, haben also 
\[\rho_1(x,0) = f(x) \]
als Anfangsbedingung gegeben

Betrachten wir die Konstante $c$ aus Gleichung \eqref{Verkehr_Schwankung}. Da
\begin{align*}
\frac{\partial \rho_1}{\partial t} &\text{ hat die Dimension } \frac{\text{Anzahl der Autos}}{\text{km} \cdot \text{h}} \\
\frac{\partial \rho_1}{\partial x} &\text{ hat die Dimension } \frac{\text{Anzahl der Autos}}{\text{km}^2} 
\end{align*}
muss $c$ die Dimension $\frac{\text{km}}{\text{h}}$ haben. Somit ist $c$ also eine Geschwindigkeit.
Wir werden nun feststellen, dass die Störungen mit genau dieser Geschwindigkeit ausbreiten. Dazu zeigen wir, dass sich entlang von Bewegungen mit Geschwindigkeit $c$ die Dichte sich nicht ändert.

Betrachten wir also eine Bewegung $x = x(t)$ und betrachten die Dichte entlang dieser Bewegung, also $\rho_1(x(t), t)$. Dann gilt für diese Dichte.
\begin{equation}
\frac{d \rho_1(x(t), t)}{\partial t} = \frac{\partial \rho_1}{\partial x} \cdot \frac{\partial x}{\partial t} + \frac{\partial \rho_1}{\partial t} \label{Lec17_3}
\end{equation}
Nehmen wir nun an, dass $x(t)$ so gewählt ist, dass $x'(t) = c$ ist dann erhalten wir:
\begin{equation*}
\frac{d \rho_1}{d t} \stackrel{\eqref{Lec17_3}}{=} \frac{\partial \rho_1}{\partial x} \cdot c + \frac{\partial \rho_1}{\partial t} \stackrel{\eqref{Verkehr_Schwankung}}{=} 0 
\end{equation*}
Das heißt also, wenn wir lineare Bewegungen
\[x(t) = x_0 + c t\]
betrachten, dann ändert sich entlang dieser Bewegungen die Dichte (bzw. genauer die Störung $\rho_1$) nicht. Die Dichte-Störungen bewegen sich also genau auf diesen Geraden mit Geschwindigkeit $c$.
Wir haben also
\begin{equation}
\rho_1(x_0 + ct, t) = \rho_1(x_0, 0) = f(x_0)
\end{equation}

Um die Dichte-Störung $\rho_1$ zu einem Zeitpunkt $t$ an einem beliebigen Ort $x$ zu berechnen, muss einfach \glqq rückwärts\grqq{} entlang der Geraden gehen, also die Nullstelle $x_0$ der Geraden $x(t)$ berechnen, welche den Punkt $(x,t)$ schneidet. Es gilt also
\begin{equation}
\rho_1(x, t) = \rho_1(x - ct, 0) = f(x - ct) \label{Loesung_LinDGL}
\end{equation}
\begin{center}
\begin{tikzpicture}
\draw[thick, ->] (0,0) -- (5,0) node[anchor=west] {$x$};
\draw[thick, ->] (0,0) -- (0,5) node[anchor=south] {$t$};

\draw[thick, Blue] (1,0.1) -- (1,-0.1) node[anchor=north] {$x_0 = x - ct$};

\draw[Gray] (0,2.5) -- (1,5);
\draw[Gray] (0,0) -- (2,5);
\draw (1,0)  -- (2,2.5) node[anchor=west] {$x_0 = x - ct$} -- (3,5);
\draw[Gray] (2,0) -- (4,5);
\draw[Gray] (3,0) -- (5,5);
\draw[Gray] (4,0) -- (5,2.5);

\fill[Blue] (2.5,3.75) circle(0.08) node[anchor=west] {$(x,t)$};
\draw[Blue, dashed, thick] (2.5,3.75) edge[bend right=20, ->] (1,0);
\end{tikzpicture}
\end{center}
\textbf{Fazit:}\\
$\rho_1$ breitet sich als Welle mit Wellengeschwindigkeit $c$ aus. Die Wellengeschwindigkeit ist nicht abhängig von der Geschwindigkeit der Autos. \\
Die Kurve $\{(x,t) | x =  x_0 + ct \}$ entlang derer die Dichte $\rho_0$ konstant ist nennt man \textbf{Charakteristiken} der Differentialgleichung \eqref{Verkehr_Schwankung}.
\begin{ex}
\begin{center}
\begin{minipage}{6cm}
\begin{align*}
\rho_t + 2 \rho_x &= 0 \\
\rho_t(x,0) &=
\begin{cases}
0 & x < 0 \\
4x & 0 \leq x \leq 1 \\
0 & x > 1
\end{cases}
\end{align*}
\end{minipage}
\begin{minipage}{4cm}
\begin{tikzpicture}
\draw[thick, ->] (0,0) -- (3.5,0) node[anchor=west] {$x$};
\draw[thick, ->] (0,0) -- (0,2.5) node[anchor=south] {$\rho(x,0)$};
\foreach \x in {0,...,3}
    \draw (\x,0.1) -- (\x,-0.1) node [below] {\x};

\foreach \x in {0,...,4}
    \draw (0.1,0.5*\x) -- (-0.1,0.5*\x) node [left] {\x};

\draw[blue] (0,0) -- (1,2)
				(1,0) -- (3.5,0);
\end{tikzpicture}
\end{minipage}
\end{center}
Die Charakteristiken sind dann gegeben durch
\begin{align*}
x &= x_0 + 2t \\
\rho_t(x,t) &=
\begin{cases}
4(x - 2t) & 2t \leq x \leq  2t + 1 \\
0 & \text{sonst}
\end{cases}
\end{align*}
\end{ex}
\begin{rem}
Im $t,x$-Diagramm sind die Charakteristiken die Geraden mit der Steigung $\frac{1}{c}$
\end{rem}

\textit{Erinnerung}:\\
Aus unseren Annahmen an die Gestalt von $q$ in den Gleichungen \eqref{Fluss_Dichte_Geschw} sowie in \eqref{Fluss_konkav} folgt, dass $c$ sowohl negativ als auch positiv sein kann. $c$ wechselt das Vorzeichen genau bei $\rho_0 = \rho_c$.

\textbf{Ausbreitung linearer Dichtewellen} \\
\begin{minipage}{7cm}
\begin{tikzpicture}
\draw[fill=red!10, thick] (2,0) -- (4,0) arc (0:90:2);
\draw[fill=blue!10, thick] (2,0) -- (2,2) arc (90:180:2);

\draw[thick, ->] (0,0) -- (4.5,0) node[anchor=west] {$\rho$};
\draw[thick, ->] (0,0) -- (0,2.5) node[anchor=south] {$q$};

\draw[thick] (2,0.1) -- (2,-0.1) node[below] {$\rho_c$};

\draw[blue] (1.25,0.75) -- (0.3,-0.5) node[below] {leichter Verkehr};
\draw[red] (2.75,0.75) -- (3.7,-0.5) node[below] {schwerer Verkehr};
\end{tikzpicture}
\end{minipage}
\begin{minipage}{8cm}
\vspace{0.3cm}
\textbf{schwerer Verkehr}\\
Dichte ist größer als $\rho_c$, hier bewegen sich die Störungen mit einer negativen Geschwindigkeit $c$ (denn $q$ ist hier fallend) \\
\textbf{leichter Verkehr}\\
Dichte ist kleiner als $\rho_c$, hier bewegen sich die Störungen mit positiver Geschwindigkeit (denn $q$ ist hier steigend) 
\end{minipage}

\begin{ex}
Angenommen wir haben schweren Verkehr bei einer uniformen Dichte. Die Anfangsdichte habe folgende Form mit leichten Abweichungen von einer konstanten Verkehrsdichte:\\
\begin{center}
\begin{tikzpicture}
\draw[thick, ->] (-5,1) -- (5,1) node[right] {$x$};

\draw[dashed, blue!40] (-5,2) -- (5,2);
\draw[dashed] (0,1) -- (0,3);
\draw[thick] (0,1.1) -- (0,0.9) node[below] {$0$};

\draw[blue] (-5,2) -- (-4.5,2) edge[bend left=10] (-3,1.7);
\draw[blue] (-3,1.7) edge[bend right=40] (-2,2.1);
\draw[blue] (-2,2.1) edge[bend left=30] (-1,2.4);
\draw[blue] (-1,2.4) edge[bend left=10] (0,1.8);
\draw[blue] (0,1.8) edge[bend right=20] (0.5,2.1);
\draw[blue] (0.5,2.1) edge[bend left=10] (0.7,2.25);
\draw[blue] (0.7,2.25) edge[bend left=10] (1.0,2.15);
\draw[blue] (1.0,2.15) edge[bend right=15] (1.3,2.15);
\draw[blue] (1.3,2.15) edge[bend left=25] (1.8,2.1);
\draw[blue] (1.8,2.1) edge[bend right=25] (2.5,1.9);
\draw[blue] (2.5,1.9) edge[bend right=25] (4,2);
\draw[blue] (4,2) node[above] {$\rho(x,0)$} -- (5,2);

\draw (-4, 2.5) node[above] {$t=0$};

\draw[thick, ->] (-5,-3) -- (5,-3) node[right] {$x$};

\draw[dashed, blue!40] (-5,-2) -- (5,-2);
\draw[dashed] (0,-3) -- (0,-1);
\draw[thick] (0,-2.9) -- (0,-3.1) node[below] {$0$};

\draw[dashed, red!40] (-1.5,-3) -- (-1.5,-1);
\draw[thick, red] (-1.5,-2.9) -- (-1.5,-3.1) node[below] {$-c \cdot t$};

\draw[blue] (-5,-2.1) edge[bend left=10] (-4.5,-2.3);
\draw[blue] (-4.5,-2.3) edge[bend right=40] (-3.5,-1.9);
\draw[blue] (-3.5,-1.9) edge[bend left=30] (-2.5,-1.6);
\draw[blue] (-2.5,-1.6) edge[bend left=10] (-1.5,-2.2);
\draw[blue] (-1.5,-2.2) edge[bend right=20] (-1,-1.9);
\draw[blue] (-1,-1.9) edge[bend left=10] (-0.8,-1.75);
\draw[blue] (-0.8,-1.75) edge[bend left=10] (-0.5,-1.85);
\draw[blue] (-0.5,-1.85) edge[bend right=15] (-0.2,-1.85);
\draw[blue] (-0.2,-1.85) edge[bend left=25] (0.3,-1.9);
\draw[blue] (0.3,-1.9) edge[bend right=25] (1,-2.1);
\draw[blue] (1,-2.1) edge[bend right=25] (2.5,-2);
\draw[blue] (2.5,-2) -- (4,-2) node[above] {$\rho(x,t)$} -- (5,-2);

\draw (-4, -1.5) node[above] {$t>0$};

\draw[->, black!50] (0,1) -- (-1.5,-1);
\end{tikzpicture}
\end{center}
Die Störung zum Zeitpunkt $t$ ist um $c \cdot t$ in negativer Richtung verschoben.
\\
Autos die gerade in einem dichten Autopulk sind, werden bremsen. Das dahinter fahrende Fahrzeug kommt somit in eine Region mit größerer Dichte. Diese Art der \glqq Kettenreaktion\grqq{} wandert nun weiter.
\end{ex}