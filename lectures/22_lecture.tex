\marginpar{05.01.2016}
\subsubsection{Referenzen}
R. Le Veque: \textit{Numerical Methods for Conservation Laws}, Lecture Notes in Mathematics, Birkhäuser 1992 \\
Treiber, Kesting: \textit{Verkehrsdynamik und Simulation}, Springer 2010 \\
Courant, Friedrichs, Lewy: \textit{Über die partiellen Differenzengleichungen in der mathematischen Physik}, Mathematische Annalen (100), 1928, \textit{p. 32-74}

\subsection{Mikroskopische Verkehrssimulation}
Bisher haben wir die Verkehrssimulation aus \textit{makroskopischer Sicht} betrachtet. Der dabei untersuchte Verkehrsfluss wurde dabei analog zu Gasen oder Flüssigkeiten modelliert und wir haben hydrodynamische Modelle benutzt. \\
Solche Makromodelle sind geeignet, wenn
\begin{itemize}
\item sich Eigenschaften nur schwer mikroskopisch beschreiben lassen
\item Details zum Beispiel Fahrzeugtyp oder Ähnliches nicht relevant sind
\item wenn die zur Verfügung stehenden Datenquellen heterogen sind
\end{itemize}
Bei der \textbf{mikroskopischen Sicht} gehen wir dagegen von einzelnen \textit{Fahrzeug-Fahrer\-modellen} aus. Damit wird der Einfluss einzelner Fahrzeuge auf den Verkehr modelliert, zum Beispiel dem Einfluss von Fahrerassistenzsystemen. Die mikroskopische Modellierung ist insbesondere bei Aufgabenstellungen wichtig, bei denen die Heterogenität des Verkehrs wichtige Rolle spielt. Dies ist zum Beispiel bei der Simulation von Auswirkungen eines Tempolimits oder eines Überholverbotes. Außerdem ermöglicht die mikroskopische Sicht die Beschreibung menschlichen Fahrverhaltens und verschiedener Fahrstile sowie der Interaktion zwischen unterschiedlichen Verkehrsteilnehmer (zum Beispiel PKW, LKW, Bus, Fußgänger, Radfahrer).

Als Verbindung zwischen mikroskopischen und makroskopischen Ansätzen betrachten wir \textbf{mesoskopische Modelle} in denen zum Beispiel die Parameter des Mikromodells von makroskopischen Größen abhängt (z.B. Verkehrsdichte) oder makroskopische Größen wie Staubeginn/ende von mikroskopischen Ratengleichungen abhängen.

\subsubsection{Simulation mittels zellulärer Automaten}
Zelluläre Automaten beschreiben dynamische Systeme ausschließlich durch ganzzahlige Variablen. \\
Der physikalische Ort $x$ wird dabei in \textbf{Zellen} der Länge $\Delta x$ mit Index $i$ eingeteilt. \\
Die zeitliche Länge bezeichnen wir mit $\Delta t$. \\
Eine Zelle $i$ kann zu einem Zeitpunkt $t$  im Zustand $\rho_i(t)=1$ \textit{(besetzt)} oder $\rho_i(t) = 0$ \textit{(leer)} sein. Weitere Variablen wie zum Beispiel die Geschwindigkeit $v_i(t)$ können ebenfalls der Zelle zugeordnet werden. $x_i(t)$ ist dabei der ganzzahlige Zustand, dass am Ort $i \Delta x$ zur Zeit $t$ ein Fahrzeug die mittlere Geschwindigkeit $v_\text{phy} = v \frac{\Delta x}{\Delta t}$.
\begin{ex}
Wir betrachten die maximale Geschwindigkeit im Standverkehr: $50 \frac{\text{km}}{\text{h}}$. Diese diskretisieren wir mit einem zellulären Automaten in $10 \frac{\text{km}}{\text{h}}$ Schritten. Dann entspricht die Fortbewegung eines Fahrzeugs mit $v_{\max} = 5 \frac{\text{Zellen}}{\text{Zeitschritt}}$ diesen $50 \frac{\text{km}}{\text{h}}$. \\
Damit ist der Zeitschritt $\Delta t$ festgelegt, denn aus
\[ 5 \cdot \frac{7{,}5}{\Delta t} \text{m} = 50 \frac{\text{km}}{\text{h}} \]
wobei $7{,}5$m die durchschittliche Länge eines Autos ist, folgt, dass $\Delta t = 2{,}7$s. \\
Jedoch ist $\Delta t =  2{,}7$s zu lang. Besser wäre $\Delta t = 1$s, dann wäre die Geschwindigkeit der Fortbewegung mit einer Zelle pro Zeiteinheit ungefähr $27 \frac{\text{km}}{\text{h}}$.
\end{ex}
Als zentrale Modellannahmen haben wir
\begin{itemize}
\item[\bf Kollisionsfreiheit] ~\\
Zwei Fahrzeuge dürfen sich nicht in der gleichen Zelle befinden. Ein Fahrzeug muss also verzögerungsfrei abbremsen können.
\item[\bf Erhaltung der Fahrzeuge] ~\\
Es dürfen keine Fahrzeuge verloren gehen.
\end{itemize}
Die Fahrzeuge seien in Fahrtrichtung nummeriert, das heißt Fahrzeug $i$ fährt mit Geschwindigkeit $v_i$ hinter $i+1$ mit $v_{i+1}$. Sei $d(i,j)$ der Abstand zwischen Fahrzeugen $i$ und $j$. Dann können wir folgenden Algorithmus angeben.

\begin{algorithm}[H]
	\begin{algorithmic}[1]
	\STATE $v_i \leftarrow \min \{v_i +1, v_{\max} \}$ \hfill \textit{(Beschleunigen)}
	\STATE $v_i \leftarrow d(i, i+1)$ falls $v_i > d(i, i+1)$ \hfill \textit{(Bremsen)}
	\STATE Fahrzeug $i$ bewegt sich $v_i$ Zellen vorwärts \hfill \textit{(Bewegen)}
	\end{algorithmic}
	\caption{ZA-Modell Algorithmus}
	\label{alg:ZA}
\end{algorithm}	
Wir erkennen, dass die algorithmische Umsetzung dieses Modells sehr einfach ist. Das Problem liegt in der Wahl der Diskretisierung und der Umsetzung von Details in der Simulation.