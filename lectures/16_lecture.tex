\marginpar{01.12.2015}
\subsubsection*{Zusammenhang von Geschwindigkeit, Fluss und Dichte}
Wir wollen nun einen Zusammenhang zwischen Geschwindigkeit, Fluss und Dichte herstellen.
\begin{ex}
Nehmen wir an, dass die Fahrzeuge eine konstante Geschwindigkeit $v_0$ haben und die Verkehrsdichte ebenfalls konstant ist mit dem Wert $\rho_0$.

Ein Auto kann dann den Beobachter am Ort $x$ gerade noch am Ende von $\tau$ Stunden passieren, falls es zur Zeit $0$ mit der Entfernung von nicht mehr als $v_0 \cdot \tau$ Kilometern vom Beobachter entfernt war. 
Damit ist die Anzahl der Autos, die den Beobachter in $\tau$ Stunden passieren genau $\rho_0 v_0 \tau$. Der Verkehrsfluss  beträgt dann $q = \rho_0 v_0$ \glqq Autos pro Stunde\grqq{}.
\end{ex}

Verallgemeinern wir dies nun auf nicht konstante Geschwindigkeiten $v$ und Dichten $\rho$. Dazu betrachten wir nun nur ein hinreichend kleines Zeitintervall $\Delta t$:\\
Die Anzahl der Autos, die in $\Delta t$ den Beobachter am Ort $x$ passieren ist $\rho(x,t) v(x,t) \Delta t$. Daraus kann man nun ableiten, dass der Verkehrsfluss gegeben ist, durch
\begin{equation}
q(x,t) = \rho(x,t) \cdot v(x,t) \label{Fluss_Dichte_Geschw}
\end{equation}

\subsubsection*{Erhaltung der Autos}

Die Verkehrsdichte $(x,t) \mapsto \rho(x,t)$ sei stetig. Dann ist die Anzahl der Autos zwischen $x = a$ und $x = b$ offensichtlich gegeben durch
\begin{equation}
N(t) = \int_a^b \rho(x,t) dx \label{Anzahl_Autos}
\end{equation}
Da keine Autos verloren gehen können, kann sich die Zahl der Autos im Abschnitt zwischen $a$ und $b$ nur über die Randpunkte ändern. Es gilt also
\begin{equation}
\frac{dN(t)}{dt} = q(a,t) - q(b,t) \label{Veraenderung_Anzahl_Autos}
\end{equation}
Nimmt man nun \eqref{Anzahl_Autos} und \eqref{Veraenderung_Anzahl_Autos} zusammen, erhält man
\begin{thm}[Integraler Erhaltungssatz] \label{Thm_Integraler_Erhaltungssatz}
Für alle Streckenabschnitte $[a,b]$ mit $a \leq b$ gilt, dass sich die Zahl der Autos nur über den Rand ändert, das heißt
\begin{equation}
\frac{d}{dt} \int_a^b \rho(x,t) dx = q(a,t) - q(b,t) \label{Integraler_Erhaltungssatz}
\end{equation}
\end{thm}

\begin{ex}
Betrachte eine unendlich lange Straße und mit $\lim_{|x| \to \infty} q(x,t) = 0$ für alle $t$ folgt dann
\begin{equation*}
\frac{d}{dt} \int_{-\infty}^{\infty} \rho(x,t) dx = 0
\end{equation*}
und deshalb
\begin{equation*}
\int_{-\infty}^{\infty} \rho(x,t) dx = const
\end{equation*}
Da die Dichte an den Rändern ($\pm \infty$) null ist, verlassen keine Autos das Gebiet $(-\infty, \infty)$ und somit ist die Zahl der Autos insgesamt konstant.
\end{ex}

Wir können Theorem \ref{Thm_Integraler_Erhaltungssatz} auch in differenzieller Form formulieren.
\begin{cor}[Differenzieller Erhaltungssatz] \label{Thm_Differenzieller_Erhaltungssatz}
An jedem Ort $x$ und zu jedem Zeitpunkt $t$ gilt
\begin{equation}
\frac{\partial \rho(x,t)}{\partial t} + \frac{\partial (\rho v(x,t))}{\partial x} = 0 \label{Differenzieller_Erhaltungssatz}
\end{equation}
\end{cor}
\begin{proof}
Theorem \ref{Thm_Integraler_Erhaltungssatz} mit $a=x$ und $b=x + \Delta x$ liefert uns
\begin{equation*}
- \frac{\partial}{\partial t}  \int_x^{x+\Delta x} \rho (\xi, t) d \xi = q(x + \Delta x, t) - q(x,t)
\end{equation*}
Division mit $\Delta x$ und $\Delta x \to 0$ bringt
\begin{equation*}
\lim_{\Delta x \to 0} - \frac{\partial}{\partial t} \frac{1}{\Delta x} \int_x^{x+\Delta x} \rho (\xi, t) d \xi = \lim_{\Delta x \to 0} \frac{q(x + \Delta x, t) - q(x,t)}{\Delta x}
\end{equation*}
Auf beiden Seiten haben wir nun den Differenzenquotienten bezüglich $x$. Mit dem Hauptsatz der Differential- und Integralrechnung ergibt sich also:
\begin{equation*}
- \frac{\partial}{\partial t} \rho(x,t) = \frac{\partial}{\partial x} q(x,t)
\end{equation*}
und damit die Behauptung.
\end{proof}
Da nach Gleichung \eqref{Fluss_Dichte_Geschw} gilt $q = \rho \cdot v$, erhalten wir dann
\begin{equation*}
\frac{\partial \rho}{\partial t} + \frac{\partial (\rho v)}{\partial x} = 0
\end{equation*}
Wäre die Geschwindigkeit $v$ bekannt, dann kann diese partielle Differentialgleichung die weitere Entwicklung der Verkehrsdichte beschreiben.
Unser nächster Schritt ist also, die Geschwindigkeit abhängig von der Dichte zu modellieren.

\begin{modeling}[Annahmen an die Geschwindigkeit und den Fluss]
Wir nehmen nun an, dass die Verkehrsgeschwindigkeit abhängig ist von der Verkehrsdichte $\rho$. Zusätzlich verlangen wir Folgendes:
\begin{enumerate}[(i)]
\item Ist die Straße leer, dann fahren die Autofahrer mit maximal (erlaubter) Geschwindigkeit $v(\rho_{\min}) = v_{\max}$
\item Die Geschwindigkeit ist monoton fallend, also $\frac{d v}{d \rho} \leq 0$. Ist die Straße befahren, dann gilt also $v(\rho) \leq v(\rho_{\min})$.
\item Ist die Straße so voll, dass \glqq Stoßstange an Stoßstange\grqq{} gilt, dann ist $v(\rho_{\max}) = 0$
\end{enumerate}
Für den Fluss $q$, der nun nach Gleichung \eqref{Fluss_Dichte_Geschw} ebenfalls nur eine Funktion von $\rho$ ist, fordern wir zusätzlich, dass
\begin{equation}
\frac{d^2 q}{d^2 \rho} < 0 \label{Fluss_konkav}
\end{equation}
\end{modeling}

Wir legen fest, dass $\rho_{max}<\frac{1}{L}$ wobei $L$ die Länge der Autos ist (der Verkehr kommt also zum Erliegen, bevor die Fahrzeuge sich berühren).

Der Verkehrsfluss ist also ebenfalls eine Funktion von $\rho$:
\begin{equation*}
q(\rho) = \rho \cdot v (\rho)
\end{equation*}
Es gilt offensichtlich
\begin{equation*}
q(x,t) = 0 \Leftrightarrow  \rho = 0 \text{ oder } \rho = \rho_{\max}
\end{equation*}
Der Verkehr kommt also vollständig zum Erliegen, wenn entweder kein Auto auf der Straße ist, oder die Dichte so hoch ist, dass die Autos Stoßstange an Stoßstange stehen.

Zusammen mit der Annahme \eqref{Fluss_konkav}, hat $q$ ein eindeutiges Maximum $q_{\max}$ bei einer Dichte $\rho_c$. Die Funktion $q$ hat also folgenden Verlauf:
\begin{center}
\begin{tikzpicture}
\draw (1.9,0) -- (3.8,0) arc (0:180:1.9);

\draw[thick, ->] (0,0) -- (4.5,0) node[anchor=west] {$\rho$};
\draw[thick, ->] (0,0) -- (0,2.5) node[anchor=south] {$q$};

\draw[thick] (3.8,0.1) -- (3.8, -0.1) node[below] {$\rho_{\max}$};
\draw[thick] (1.9,0.1) -- (1.9, -0.1) node[below] {$\rho_c$};
\draw[thick] (0.1,1.9) -- (-0.1, 1.9) node[anchor=east] {$q_{\max}$};
\end{tikzpicture}
\end{center}
Diesen Verlauf von $q$ in Abhängigkeit von $\rho$ nennt man \textbf{Fundamentaldiagramm} des Straßenverkehrs.
Das Maximum $q_{\max}$ nennen wir auch \textbf{Kapazität der Straße}.

Nun wollen wir versuchen, die Geschwindigkeit konkret abhängig von der Verkehrsdichte zu modellieren.

Betrachten wir dazu nun zwei hintereinander fahrende Autos $A_n$ (hinteres Fahrzeug) und $A_{n-1}$ (vorderes Fahrzeug) mit den jeweiligen Positionen $x_n(t)$ bzw. $x_{n-1}(t)$. Da wir davon ausgehen, dass keine Überholmöglichkeit besteht, sollte $A_n$ seine Geschwindigkeit von der Relativgeschwindigkeit
\begin{equation*}
\Delta v := \frac{d x_n (t)}{dt} - \frac{d x_{n-1} (t)}{dt}
\end{equation*}
abhängig machen.

Ist $\Delta v > 0$ heißt das, dass $A_n$ schneller ist, als das vorausfahrende Fahrzeig ist, und bremsen (bzw. negativ beschleunigen) sollte. Im umgekehrten Fall ist $A_n$ langsamer als das vorausfahrende Fahrzeug, und kann beschleunigen.
\begin{modeling}[Beschleunigung]
Wir nehmen an, dass die Autos entsprechend ihrer Relativgeschwindigkeit zum vorausfahrenden Fahrzeug beschleunigen. Wir setzen also
\begin{equation}
\frac{d^2 x_n(t)}{d^2 t} = - \lambda \left( \frac{d x_n (t)}{dt} - \frac{d x_{n-1} (t)}{dt} \right) \label{Beschleunigung_lin}
\end{equation}
wobei $\lambda > 0$ die Sensitivität modelliert. 
\end{modeling}
Integration von \eqref{Beschleunigung_lin} bezüglich $t$ bringt
\begin{equation*}
\frac{d x_n(t)}{d t} = - \lambda \left( x_n (t) - x_{n-1} (t) \right) + d_n
\end{equation*}
mit Integrationskonstante $d_n$. Die Geschwindigkeit der Autos ist also abhängig vom Abstand der Autos. Der mittlere Abstand zweier Autos ist gerade der Kehrwert der Dichte, also nutzen wir (die im Mittel korrekte) Annäherung
\begin{equation*}
x_{n-1} (t) - x_{n} (t) \approx \frac{1}{\rho(x(t),t)}
\end{equation*}

Aus dieser Modellierung erhalten wir also für unsere Verkehrsgeschwindigkeit
\begin{equation*}
v(x,t) = \frac{\lambda}{\rho(x,t)} + d_n
\end{equation*}
Wähle nun $d_n$ so, dass $\rho = \rho_{\max}$ für $v=0$ gilt, also
\[ d_n = -\frac{\lambda}{\rho_{\max}} \]
Zusammen ergibt sich also
\begin{equation*}
v(x,t) = \lambda \left( \frac{1}{\rho(x,t)} - \frac{1}{\rho_{\max}} \right)
\end{equation*}
Diese Geschwindigkeit ist allerdings unbeschränkt in $\rho$. Wir beschränken nun noch die Geschwindigkeit mit $v_{\max}$ und erhalten mit
\begin{equation}
v(t,x) =
\begin{cases}
v_{\max} & \rho \leq \rho_{crit} \\
\lambda \left( \frac{1}{\rho(x,t)} - \frac{1}{\rho_{\max}} \right) & \rho > \rho_{crit}
\end{cases}
\end{equation}
eine Geschwindigkeitsfunktion die unseren Modellierungswünnschen entspricht. Für den Fluss erhalten wir damit 
\begin{equation}
q(t,x) =
\begin{cases}
v_{\max} \rho(x,t) & \rho \leq \rho_{crit} \\
\lambda \left( 1 - \frac{\rho(x,t)}{\rho_{\max}} \right) & \rho > \rho_{crit}
\end{cases}
\end{equation}