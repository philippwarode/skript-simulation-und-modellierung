
\marginpar{25.01.2016}
\textit{Dipol(moment)}\\
Hat ein Molekül an unterschiedlichen Orten elektrische Ladung mit verschiedenen Vorzeichen, so dass der Schwerpunkt der negativen Ladungen nicht mit den positiven Ladungen übereinstimmt, dann besitzt das Molekül einen \textbf{Dipolmoment}.
\begin{ex}[Dipol im Wassermolekül]
\begin{center}
\begin{tikzpicture}
\node (1) {O};
\node[below left=1cm of 1] (2) {H};
\node[below right=1cm of 1] (3) {H};
\draw (2) edge (1)
      (1) edge (3);
\node[above=0 of 1] (4) {$\delta^-$};
\node[below=0 of 2] (5) {$\delta^+$};
\node[below=0 of 3] (6) {$\delta^+$};
\node[below=2cm of 1] (dummy) {};
\draw[red, <-] (dummy) edge (1) node {gerichteter Dipol};
\end{tikzpicture}
\end{center}
\end{ex}
Die durch den Dipol entstehenden Kräfte können mit dem Coulombpotential modelliert werden:
\begin{equation*}
U_C (r_{ij}) = \frac{Z_i Z_j}{4 \pi \epsilon_0 \epsilon r_{ij}}
\end{equation*}
Dabei ist
\begin{itemize}
\item[$Z_i$] (Partial-) Ladung
\item[$\epsilon_0$] Permeabilität des Vakuums
\item[$\epsilon$] Permeabilität des entsprechenden Mediums (Dielektrizitätskonstante)
\end{itemize}
Es gibt zwei Grundlagentypen, die Polarität des Wassers zu berücksichtigen:
\begin{itemize}
\item[\textbf{implizites Wasser}]~\\ Die Wirkung des Wassers wird durch den Parameter $\epsilon$ modelliert
\item[\textbf{explizites Wasser}]~\\ Punkte mit Ladung werden in die Simulationsbox hinzugefügt.
\end{itemize}

Zusammenfassend erhalten wir folgendes Potential
\begin{align*}
V(q) &= \\
&=\sum_{\text{Bindungen}} \frac{k_{l,i}}{2} \left( l_i - l_{i,\text{eq}} \right)^2 
+
\sum_{\text{Winkel}} \frac{k_{w,i}}{2} \left( \vartheta_i - \vartheta_{i, \text{eq}} \right)^2 \\
&\qquad +
\sum_{\text{Torsionswinkel}} \frac{V_n}{2} \left( 1+ \cos(n \alpha - \alpha_{\text{eq}}) \right) \\
&\qquad+
\underbrace{
\sum_{ij} \left(
4 \epsilon  \left( \left( \frac{\sigma}{r_{ij}} \right)^{12} - \left( \frac{\sigma}{r_{ij}} \right)^6 \right)
+
 \frac{Z_i Z_j}{4 \pi \epsilon_0 \epsilon r_{ij}}
 \right)
 }_{=:E_{NB}}
\end{align*}
Wir betrachten nun die Newtonsche Bewegungsgleichung und erhalten
\begin{equation}
M \ddot{q} = F = - \nabla \ddot{V}(q)
\end{equation}
\subsubsection*{Reduktion der Rechenzeit bei der Berechnung von $V(q)$}
\textbf{Abschneidemethoden} \\
Setze
\begin{equation*}
E_{NB}(q) =
\sum_{ij} w_{ij} S(r_{ij}) \left[
4 \epsilon  \left( \left( \frac{\sigma}{r_{ij}} \right)^{12} - \left( \frac{\sigma}{r_{ij}} \right)^6 \right)
+
 \frac{Z_i Z_j}{4 \pi \epsilon_0 \epsilon r_{ij}}
 \right]
\end{equation*}
mit Gewichten $w_{ij}$ und der Abschneidefunktion
\begin{equation*}
S_{\text{cutoff}}(r) =
\begin{cases}
1 & r \leq r_{\text{cut}} \\
0 & \text{sonst}
\end{cases}
\end{equation*}
Die Unstetigkeit bei $r_{\text{cut}}$ führt dazu, dass die Energie \textit{nicht} erhalten bleibt. Als Abhilfe betrachten wir eine Übergangszone aus den drei Abschnitten
\begin{equation*}
r < \tilde{r} \qquad \tilde{r} < r < r_{\text{cut}} \qquad r > r_{\text{cut}}
\end{equation*}
In dieser Übergangszone modellieren wir die Unstetigkeit durch ein geeignetes Polynom
\begin{equation*}
S_{poly} (r) =
\begin{cases}
1 & r < \tilde{r} \\
1 + y(r)^2 [2(y(r)) -3 ] & \tilde{r} \leq r \leq r_{\text{cut}} \\
0 & r > r_{\text{cut}}
\end{cases}
\end{equation*}
wobei $y(r) := \frac{r^2 - \tilde{r}^2}{r_{\text{cut}}^2 - \tilde{r}^2}$.
\begin{center}
\begin{tikzpicture}
\draw[->] (-0.5,0) -- (5,0) node[anchor=west] {$r$};
\draw[->] (0,-1.5) -- (0,3) node[anchor=south] {$E_{NB}$};
\draw[thick] (4,-0.1) -- (4,0.1) node[anchor=south] {$r_{\text{cut}}$};

\draw[blue, thick] plot [smooth, tension=0.4] coordinates { (0.1,3) (1,-1) (1.5,-1.3) (2.6, -0.3) (4,-0.15) };
\draw[blue, thick] (4,0) -- (5,0);
\end{tikzpicture}
\begin{tikzpicture}
\draw[->] (-0.5,0) -- (5,0) node[anchor=west] {$r$};
\draw[->] (0,-1.5) -- (0,3) node[anchor=south] {$E_{NB}$};
\draw[thick] (4,-0.1) -- (4,0.1) node[anchor=south] {$r_{\text{cut}}$};

\draw[red, thick] plot [smooth, tension=0.4] coordinates { (0.1,3) (1,-1) (1.5,-1.3) (2.6, -0.3) (4,-0.15) (4.2, 0) (5,0) };
\end{tikzpicture} \\
$E_{NB}$ mit $S_\text{cutoff}$ (blau) und $S_{poly}$ (rot).
\end{center}

Shifting-Funktionen:\\
Das Potential wird für $r < r_{\text{cut}}$ modifiziert
\begin{equation*}
S_{Sh}(r) = \left( 1 - \frac{r}{r_{\text{cut}}}\right)^2 \qquad \text{für } r < r_{\text{cut}}
\end{equation*}
Anwendung auf das Coulomb Potential
\begin{align*}
\tilde{E}(r) &= S_{sh}(r) \cdot E(r) \sim \frac{1}{r} \left( \frac{r_{\text{cut}} - r}{r_{\text{cut}}} \right)^2 = \frac{1}{r_{\text{cut}}} \frac{(r_{\text{cut}} - r)^2}{r} \\
\tilde{E}'(r) = E'(r) + \frac{1}{r_{\text{cut}}^2}
\end{align*}

\textbf{Periodische Randbedingungen}\\
Das System ist in eine Einheitszelle und besitzt unendlich viele Spiegel im Raum. Üblicherweise ist die Zellengröße $> 10 \mathring{A}$. Man kann das Coulomb-Potential dann schrieben als
\begin{equation*}
\sum_{ij} \frac{Z_i Z_j}{|r_{ij}|} = \sum_{j} Z_j \cdot \phi(q_j) 
\text{ mit } \phi(q_j) = \sum_{i} \frac{Z_j}{|r_{ij}|}
\end{equation*}
Dann kann man $\phi(q_j)$ in einer Taylorreihe entwickeln
\begin{equation*}
\phi(q_j) = \phi(0) + \nabla \phi^T q + \frac{1}{2} q^T H q + \mathcal{O} (|q|^3) \tag{Malipol-Methode}
\end{equation*}