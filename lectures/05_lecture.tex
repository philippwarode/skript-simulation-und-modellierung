\marginpar{26.10.15}
\begin{defn}[Eigenschaften von Relationen]
Eine Relation $R$ auf $A$ heißt
\begin{itemize}
\item \textbf{transitiv}, wenn 
\[ \forall x,y,z \in A : (xRy \land yRz) \Rightarrow xRZ \]
\item \textbf{reflexiv}, wenn x $xRx$ für alle $x \in A$ gilt
\item \textbf{Quasiordnung}, wenn $R$ transitiv und reflexiv ist
\item \textbf{asymmetrisch} wenn nie sowohl $xRy$ als auch $yRx$ gelten kann, d.h.
\[\forall x,y \in A: xRy \Rightarrow \lnot (yRx) \]
\item \textbf{konnex}, wenn je zwei unterschiedliche Elemente vergleichbar sind, d.h.
\[ \forall x,y \in A, x \neq y:  xRy \lor yRx \]
\end{itemize}
\end{defn}
Die Rangabbildung $r$ definiert eine \textbf{Präferenzrelation} $\rho \subset A \times A$ über
\begin{equation}
x \rho y :\Leftrightarrow r(x) < r(y) \label{pr_relation}
\end{equation}
$\rho$ ist transitiv und asymmetrisch (vgl. \glqq $<$\grqq{} auf $\N$). 
Die Menge aller so über eine Rangabbildung darstellbaren Relationen nennen wir
\begin{equation*}
P_A := \{ \rho \subset A \times A: \rho \text{ erfüllt \eqref{pr_relation} für eine Rangabbildung } r \} .
\end{equation*}
Wir können auch noch Paare hinzunehmen, die beiden den gleichen Rang haben:
\begin{equation}
x \rho^* y :\Leftrightarrow r(x) \leq r(y) \label{pr_relation2}
\end{equation}
$\rho^*$ ist transitiv, reflexiv und zusätzlich konnex (vgl. \glqq $\leq$\grqq{} auf $\N$). 
\[ \rho \subset \rho^* \subset A \times A \]

\begin{ex}
$A = \{x,y,z \}, r(x) = 1, r(y) = r(z) = 2$. Hier sehen $\rho, \rho^*$ so aus: \\
\begin{tabular}{c|ccc}
$\rho$ & $x$ & $y$ & $z$ \\ 
\hline 
$x$ & ~ & \checkmark  & \checkmark  \\ 
$y$ & ~ & ~ & ~ \\ 
$z$ & ~ & ~ & ~ \\ 
\end{tabular} \hspace{0.5cm}
\begin{tabular}{c|ccc}
$\rho^*$ & $x$ & $y$ & $z$ \\ 
\hline 
$x$ & \checkmark  & \checkmark  & \checkmark  \\ 
$y$ & ~ & \checkmark  & \checkmark  \\ 
$z$ & ~ & \checkmark  & \checkmark  \\ 
\end{tabular} 
\end{ex}
Auch ohne die Rangabbildung $r$ zu kennen, kann man aus gegebenen $\rho$ das zugehörige $\rho^*$ bestimmen:
\begin{equation*}
x \rho y \Leftrightarrow \lnot (y\rho^* x).
\end{equation*}
Die Relationen sind also zueinander \textbf{invers komplementär}.
\begin{equation*}
P^*_A = \{\rho^* \subset A \times A: \rho^* \text{ erfüllt \eqref{pr_relation2} für eine Rangabbildung }r \}
\end{equation*}

\begin{defn}
Sei eine Wählermenge $J := \{1, \dots n\}$ gegeben. Ein Entscheidungsverfahren bekommt als Eingabedaten die $n$ Präferenzen der Wähler und berechnet daraus mittels einer kollektiven Auswahlfunktion (auch als \glqq Wohlfahrtsfunktion\grqq{} bezeichnet)
\begin{equation*}
K: P^n_A := P_A \times \dots \times P_A \to P_A
\end{equation*}
eine \textbf{Präferenz der Gesamtheit}.
\end{defn}

Zwei wesentliche Bedingungen an Entscheidungsverfahren sind
\begin{itemize}
\item Die Abbildung $K$ muss total sein, d.h. jedem Element des Definitionsbereiches muss ein Bild zugeordnet werden. Es sind also beliebige Kombinationen beliebiger Präferenzen zugelassen.
\item Ergebnis der Wahl muss ebenfalls eine Relation sein in $P_A$ sein
\end{itemize}

\subsection{Bespiele für Entscheidungsverfahren}
Versuche Auswahlfunktionen zu konstruieren mit dem Ziel Verfahren zu erhalten, die besonders \glqq gerecht\grqq{} sind. Dabei ist die Entscheidung, was als gerecht anzusehen ist, keine mathematische Frage, sondern eine Frage die zum Beispiel aus juristischer oder philosophischer Sicht zu betrachten ist.
\begin{defn}[Externer Diktator]
Bei einem externen Diktator gilt für beliebiges aber festes $\rho_E \in P_A$
\begin{equation*}
K_{\rho_E}^E (\rho_1, \dots, \rho_n) = \rho_E
\end{equation*}
unabhängig von $\rho_1, \dots, \rho_n$. Dies ist offensichtlich eine Abbildung.
\end{defn}

\begin{defn}[Interner Diktator]
Bei einem internen Diktator gibt es einen Wähler $b \in \{1, \dots, n\}$ mit
\begin{equation*}
K_b^D (\rho_1, \dots, \rho_n) = \rho_b
\end{equation*}
\end{defn}

\begin{defn}[Rangaddition]
Zu jeder Individualpräferenz $\rho_i$ gibt es eine Rangabbildung $r$. Bei der Rangaddition werden Kandidaten $x$ und $y$ nach der Summe ihrer Rangzahlen bewertet:\\
Dann ist also $K^A(\rho_1, \dots , \rho_n)$ Relation $\rho$ mit
\begin{equation*}
x\rho y :\Leftrightarrow \sum_{i=1}^n r_i(x) < \sum_{i=1}^n r_i(y)
\end{equation*}
\end{defn}
\textit{Problem}: Die Summe $\sum_{i=1}^n r_i(x)$ ist im Allgemeinen keine Rangabbildung, wie das folgendes Beispiel zeigt.
\begin{ex}
\begin{minipage}[t]{3cm}
\textbf{Präferenzen:}\vspace{0.2cm}\\
\begin{tikzpicture}
%Axis
\draw[->] (0,0) -- (1.5,0);
\draw[->] (0,0) -- (0,-2);
%Axis-Label
\draw (0,-.5) node[anchor=east] {\footnotesize 1}
	  (0,-1) node[anchor=east] {\footnotesize 2}
	  (0,-1.5) node[anchor=east] {\footnotesize 3};
%Preferences
\draw (0.5,-0.5) node[circleNodeSmall] {\footnotesize x}
	  (0.5,-1) node[circleNodeSmall] {\footnotesize y}
	  (0.5,-1.5) node[circleNodeSmall] {\footnotesize z};
\draw (1,-0.5) node[circleNodeSmall] {\footnotesize y}
	  (1,-1) node[circleNodeSmall] {\footnotesize x}
	  (1,-1.5) node[circleNodeSmall] {\footnotesize z};
\end{tikzpicture}
\end{minipage}
\begin{minipage}[t]{3cm}
\textbf{Rangaddition:}\vspace{0.2cm}\\
\begin{tikzpicture}
%Axis
\draw[->] (0,0) -- (1.5,0);
\draw[->] (0,0) -- (0,-3.5);
%Axis-Label
\draw (0,-.5) node[anchor=east] {\footnotesize 1}
	  (0,-1) node[anchor=east] {\footnotesize 2}
	  (0,-1.5) node[anchor=east] {\footnotesize 3}
	  (0,-2) node[anchor=east] {\footnotesize 4}
	  (0,-2.5) node[anchor=east] {\footnotesize 5}
	  (0,-3) node[anchor=east] {\footnotesize 6};
%Preferences
\draw (0.5,-1.5) node[circleNodeSmall] {\footnotesize x}
	  (1,-1.5) node[circleNodeSmall] {\footnotesize y}
	  (0.5,-3) node[circleNodeSmall] {\footnotesize z};
\end{tikzpicture}
\end{minipage}
\begin{minipage}[t]{6cm}
\textbf{Mögliche Lösung:}\vspace{0.2cm}\\
\begin{tikzpicture}
%Axis
\draw[->] (0,0) -- (1.5,0);
\draw[->] (0,0) -- (0,-1.5);
%Axis-Label
\draw (0,-.5) node[anchor=east] {\footnotesize 1}
	  (0,-1) node[anchor=east] {\footnotesize 2};
%Preferences
\draw (0.5,-0.5) node[circleNodeSmall] {\footnotesize x}
	  (1,-0.5) node[circleNodeSmall] {\footnotesize y}
	  (0.5,-1) node[circleNodeSmall] {\footnotesize z};
\end{tikzpicture}
\end{minipage} \\
Wir erkennen, das die durch die Rangaddition entstehende Abbildung nicht surjektiv ist. Durch eine geeignete Verschiebung kann allerdings wieder eine Rangabbildung erzeugt werden.
\end{ex}

\begin{defn}[Cordocet-Verfahren]
Vergleicht man zwei Kandidaten $x$ und $y$ so definiere
\begin{itemize}
\item Menge, die $x$ bevorzugt: $\{i \in J : x \rho_i y \}$
\item Menge, die $y$ bevorzugt: $\{i \in J : y \rho_i x \}$
\item Menge, die indifferent ist bzgl. $x$ und $y$.
\end{itemize}
Fragestellung: Welcher der beiden Kandidaten gewinnt mehr Vergleiche? Definiere die \textbf{kollektive Präferenzrealtion} $\rho$ mit
\begin{equation}
x \rho y :\Leftrightarrow |\{i \in J : x \rho_i y\} | > |\{i \in J : y \rho_i x\} |
\end{equation}
\end{defn}

\begin{ex}[Demokratische Familie]
Wähler: Vater $V$, Mutter $M$, Kind $K$\\
Alternativen: Spielplatz $S$, Basteln $B$ oder Mittagsschlaf $MS$\\
Präferenzen:
\begin{itemize}
\item[Vater] $M \rho_V S \rho_V B$
\item[Mutter] $B \rho_M MS \rho_V S$
\item[Kind] $S \rho_V B \rho_V MS$
\end{itemize}
Familienentscheidung durch paarweise Abstimmung:
\begin{itemize}
\item[$MS$ vs $S$:] $2:1 \Rightarrow MS \rho_F S$
\item[$S$ vs $B$:] $2:1 \Rightarrow S \rho_F B$
\item[$B$ vs $MS$:] $2:1 \Rightarrow B \rho_F MS$
\end{itemize}
Wir erkennen im Resultat einen \textit{intransitiven Zykel}.
\end{ex}

\begin{ex}[Einstimmigkeit]
Setze Kandidat $x$ vor $y$, wenn alle Wähler $x$ vor $y$ setzen. Die kollektive Präferenzfunktion ist also
\begin{equation*}
x \rho y :\Leftrightarrow \forall i \in \{1, \dots, n\} x \rho_i y
\end{equation*}
Wenn aber ein Wähler $y$ genauso wie $x$ schätzt, dann gilt
\begin{equation*}
\exists i \{1, \dots, n\} : y \rho_i^* x \Leftarrow \exists i \in \{1, \dots, n \} : \lnot(x \rho_i y) \Leftrightarrow \lnot(x \rho y) \Leftrightarrow (y \rho_i x)
\end{equation*}
Das Verfahren ist also in der Praxis kaum anwendbar. Das Ergebnis muss für Kandidatenmenge $A$ mit $|A|>2$ nicht unbedingt in $P_A$ sein.
\end{ex}

\begin{ex}[Einstimmigkeitsprinzip, das keine Präferenzrelation aus $P_A$ ist]
3 Kandidaten: $A = \{x,y,z\}$\\
zwei Wähler: $J = {1,2}$ mit Rangabbildungen: 
\begin{center}
\begin{tabular}{c|c|c|c} 
$i$ & $r_i(x)$ & $r_i(y)$ & $r_i(z)$ \\ 
\hline 
1 & 1 & 2 & 3 \\ 
\hline 
2 & 3 & 1 & 2 \\  
\end{tabular} 
\end{center}
In beiden Fällen ist $y$ vor $z$, die Einstimmigkeitsrelation $\rho$ genau ein Paar $x \rho z$. Damit ist $\rho \notin P_A$, denn es ist nicht möglich, eine zugehörige Rangabbildung zu konstruieren.
\end{ex}

\begin{defn}[Borda-Wahl]
Jeder Wähler gibt den $k$ Kandidaten Punkte. Erstpräferenzen weist er $k$ Punkte, Zweitpräferenzen $k-1$ Punkte usw. zu. \\
Der Kandidat mit dem meisten Punkten gewinnt die Wahl.
\end{defn}